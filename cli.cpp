/*
Copyright (c) 2022 George Bartolomey 

Этот файл — часть w2midi_qt.

w2midi_qt — свободная программа: вы можете перераспространять ее и/или изменять ее на условиях Стандартной общественной лицензии GNU в том виде, в каком она была опубликована Фондом свободного программного обеспечения; версии 3.

w2midi_qt распространяется в надежде, что она будет полезной, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее см. в Стандартной общественной лицензии GNU.

Вы должны были получить копию Стандартной общественной лицензии GNU вместе с этой программой. Если это не так, см. <https://www.gnu.org/licenses/>.
*/

#include "cli.h"
#include <QProcess>
#include <QtCore>

W2MidiCli::W2MidiCli(QObject *parent) : QObject(parent)
{
    bufferSize = 4096;
    ampThreshold = 20;
    accThreshold = 0.02;
    clientName = "w2midi";
    rangeStart = 20;
    rangeEnd = 240;
    process = new QProcess(this);
    connect(process, &QProcess::stateChanged, this, [this](QProcess::ProcessState){
        emit processStateChanged(getProcessState());
    });
    connect(process, &QProcess::errorOccurred, this, [this](QProcess::ProcessError) { emit processStateChanged(false); });
    connect(process, &QProcess::readyReadStandardError, this, &W2MidiCli::readyReadStderror);
    setParent(parent);
}

W2MidiCli::~W2MidiCli()
{
    stop();
}

void W2MidiCli::readyReadStderror()
{
    QString err = process->readAllStandardError();
    emit errorOccurred(err);
}


void W2MidiCli::start()
{
    process->start("w2midi", {
        "-b", QString::number(bufferSize),
                   "-a", QString::number(accThreshold),
                   "-d", QString::number(ampThreshold),
                   "-n", clientName,
                   "-s", QString::number(rangeStart),
                   "-e", QString::number(rangeEnd),
    });
}

void W2MidiCli::stop()
{
    if (getProcessState()) {
        process->terminate();
        process->waitForFinished();
    }
}

void W2MidiCli::restart()
{
    stop();
    start();
}

float W2MidiCli::getAccThreshold() {
    return accThreshold;
}
float W2MidiCli::getAmpThreshold()
{
    return ampThreshold;
}
int W2MidiCli::getBufferSize()
{
    return bufferSize;
}
QString W2MidiCli::getClientName()
{
    return clientName;
}

void W2MidiCli::setAccThreshold(float a)
{
    accThreshold = a;
}
void W2MidiCli::setAmpThreshold(float a)
{
    ampThreshold = a;
}
void W2MidiCli::setBufferSize(int s)
{
    bufferSize = s;
}
void W2MidiCli::setClientName(QString name)
{
    clientName = name;
}

void W2MidiCli::setRangeStart(int v)
{
    if (v < 0)
        v = 0;
    if (rangeStart != v)
        emit rangeStartChanged(v);
    rangeStart = v;
    if (rangeStart > rangeEnd)
        setRangeEnd(rangeStart);
}

void W2MidiCli::setRangeEnd(int v)
{
    if (v > 255)
        v = 0;
    if (rangeEnd != v)
        emit rangeEndChanged(v);
    rangeEnd = v;
    if (rangeEnd < rangeStart)
        setRangeStart(rangeStart);
}

bool W2MidiCli::getProcessState()
{
    return process->state() != QProcess::NotRunning;
}

