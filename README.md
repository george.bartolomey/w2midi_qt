# w2midi_qt

A program that transforms an audio stream into MIDI!

## Screenshots

![Window](screenshots/1.png "Main window")

## Building

Install dependencies:

* C++ compiler
* Qt5 Widgets
* Qt5 Core
* [w2midi](https://notabug.org/george.bartolomey/w2midi)

Make directory build and run `cmake && make` there.
To install run `make install`. Use and enjoy!

