<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>App</name>
    <message>
        <location filename="../app.cpp" line="34"/>
        <source>Buffer size</source>
        <translation>Размер буфера</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="35"/>
        <source>samples</source>
        <translation>семплов</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="38"/>
        <source>Amplitude threshold</source>
        <translation>Порог срабатывания</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="38"/>
        <source>decibels</source>
        <translation>Дб</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="40"/>
        <source>Accuracy threshold</source>
        <translation>Максимально допустимая точность</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="43"/>
        <source>Client name</source>
        <translation>Имя клиента</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="47"/>
        <location filename="../app.cpp" line="74"/>
        <source>Start</source>
        <translation>Старт</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="48"/>
        <source>Apply</source>
        <translation>Применить</translation>
    </message>
    <message>
        <location filename="../app.cpp" line="72"/>
        <source>Stop</source>
        <translation>Стоп</translation>
    </message>
</context>
</TS>
