#pragma once

/*
Copyright (c) 2022 George Bartolomey 

Этот файл — часть w2midi_qt.

w2midi_qt — свободная программа: вы можете перераспространять ее и/или изменять ее на условиях Стандартной общественной лицензии GNU в том виде, в каком она была опубликована Фондом свободного программного обеспечения; версии 3.

w2midi_qt распространяется в надежде, что она будет полезной, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее см. в Стандартной общественной лицензии GNU.

Вы должны были получить копию Стандартной общественной лицензии GNU вместе с этой программой. Если это не так, см. <https://www.gnu.org/licenses/>.
*/

#include <QtCore>

class W2MidiCli : public QObject {
    Q_OBJECT
    int bufferSize, rangeStart, rangeEnd;
    float ampThreshold, accThreshold;
    QString clientName;
    QProcess *process;
public:
    W2MidiCli(QObject*);
    ~W2MidiCli();
    int getBufferSize();
    float getAmpThreshold();
    float getAccThreshold();
    QString getClientName();
    bool getProcessState();
private slots:
    void readyReadStderror();
public slots:
    void setBufferSize(int);
    void setAmpThreshold(float);
    void setAccThreshold(float);
    void setClientName(QString);
    void setRangeStart(int);
    void setRangeEnd(int);
    void stop();
    void start();
    void restart();
signals:
    void errorOccurred(QString);
    void processStateChanged(bool);
    void rangeStartChanged(int);
    void rangeEndChanged(int);
};
