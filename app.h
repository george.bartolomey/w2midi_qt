#pragma once

/*
Copyright (c) 2022 George Bartolomey 

Этот файл — часть w2midi_qt.

w2midi_qt — свободная программа: вы можете перераспространять ее и/или изменять ее на условиях Стандартной общественной лицензии GNU в том виде, в каком она была опубликована Фондом свободного программного обеспечения; версии 3.

w2midi_qt распространяется в надежде, что она будет полезной, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее см. в Стандартной общественной лицензии GNU.

Вы должны были получить копию Стандартной общественной лицензии GNU вместе с этой программой. Если это не так, см. <https://www.gnu.org/licenses/>.
*/

#include <QtWidgets>
#include "cli.h"

class App : public QWidget {
    Q_OBJECT
    bool isStarted;
    QLabel *log;
    W2MidiCli *cli;
    QSlider *bufferSize;
    QDial *accThreshold;
    QDial *ampThreshold;
    QPushButton *applyButton;
    QPushButton *startstopButton;
    int minBufferSizePower;
    void enableApply();
    void startStop();
public:
    App();
};
